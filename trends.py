from pytrends.request import TrendReq
import sys

def get_csv(term, nums):
    to_return = term

    for e in nums:
        to_return += ",{}".format(e)

    return to_return + "\n"


def trend_link(terms, f):
    new_list = []
    for e in terms:
        new_list.append([e,terms[0]])

    for e in new_list:
        f.write(trend_it(e))

    f.write("\n")

        
def trend_it(term):
    kw_list = term
    pytrends.build_payload(kw_list, cat=0, timeframe='today 5-y', geo='US', gprop='')
    nums = []
    x = pytrends.interest_over_time()
    for index, row in x.iterrows():
        nums.append(row[0])

    return get_csv(term[0], nums)

if __name__ == "__main__":

    
    pytrends = TrendReq(hl='en-US', tz=360)

    terms = []

    Hives = ["Honey Flow", "Better Bee", "Bee Built", "Dadant", "Backyard Hive"]

    Honey = ["nature nates","o organics", "wedder spoon", "manuca doctor", "convita", "suebee", "glorybee"]

    Bath = ["plant therapy", "nature soil", "aura cacia", "nature's answer", "mountain rose herbs", "organix", "honest", "better skin co.", "L'occitane", "josie maran cosmetics"]

    terms.append(Hives)
    terms.append(Honey)
    terms.append(Bath)

    f = open(sys.argv[1],"w")

    for e in terms:
        trend_link(e, f)
    
    """ OLD QUERIES
    Bath = ["Soap Bases", "Butters", "Palm Oils", "Oilve Oil", "Lip Balm", "Soap Making","Essential Oils"]

    Honey = ["Domestic Clover Honey", "Blackberry Honey", "Save the Bee Honey", "Coffee Blossom Honey", "Organic Tropical Blossom Honey", "Montana White Clover Honey", "Organic Clover Honey", "Meadowfoam Honey", "HoneyStix"]

    Nutri = ["Royal Jelly", "Organic Bee Pollen"]

    Bee = ["Beekeeping Kit", "Beekeeping Suit", "Assembled Hives", "Honey Harvesting", "Upscale Hives"]
    

    terms = []
    terms.append(Bath)
    terms.append(Honey)
    terms.append(Nutri)
    terms.append(Bee)
    """

    #terms = [["Amazon", "Walmart", "iherb", "thrive market", "bulk apothecary", "boxed", "costco", "gnc", "vitamin shoppe", "Hello Fresh"], ["iherb", "thrive market", "bulk apothecary", "boxed", "costco", "gnc", "vitamin shoppe", "Hello Fresh"]]
