### What is this repository for? ###

Trending terms using Google Trends. Output is in CSV format.

### How To Use ###
Requires the pytrends package: https://github.com/GeneralMills/pytrends#interest-over-time
The pytrends package can be installed using the command "pip install pytrends"

Example Usage:
"python2.7 trends.py competitor_trends.csv", where competitor_trends.csv is the output file.

To run your own trends, create lists of strings you want to trend together. Add all of these lists to the "term" list using term.append(NAME_OF_YOUR_LIST)

